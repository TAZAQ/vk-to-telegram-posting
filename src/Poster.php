<?php
namespace VKTOTG;

class Poster implements IPoster
{
    private $bot_api_token;
    private $bot_username;
    private $chat_id;

    public $owner_id;
    public $vk_public_url;
    public $date;
    public $text;
    public $repost;
    public $attachments;


    public function __construct($bot_api_token, $bot_username, $chat_id)
    {
        $this->setBotApiToken($bot_api_token);
        $this->setBotUsername($bot_username);
        $this->setChatId($chat_id);
    }

    
    /**
     * @param mixed $bot_api_token
     */
    public function setBotApiToken($bot_api_token): void
    {
        $this->bot_api_token = $bot_api_token;
    }

    /**
     * @param mixed $bot_username
     */
    public function setBotUsername($bot_username): void
    {
        $this->bot_username = $bot_username;
    }

    /**
     * @param mixed $chat_id
     */
    public function setChatId($chat_id): void
    {
        $this->chat_id = $chat_id;
    }



    /**
     * Метод присвоения данных из $data
     * @param $data - массив (json) от ВК
     */
    function getData($data)
    {
        $owner_id = abs($data['owner_id']);
        $this->vk_public_url = "https://vk.com/public{$owner_id}?w=wall-{$owner_id}_{$data['id']}";
        $this->owner_id = $data['owner_id'];
        $this->date = $data['date'];
        $this->text = $data['text'];
        $this->repost = $this->parseRepost($data['copy_history']);
        $this->attachments = $this->parseAttachments($data['attachments']);
    }

    /**
     * Метод парсинга репоста (если таковой имеется)
     * @param $repost
     * @return array|null
     */
    private function parseRepost($repost) {
        if (! $repost) return null;

        $owner_id = abs($repost['owner_id']);
        return [
            '' => "https://vk.com/public{$owner_id}?w=wall-{$owner_id}_{$repost['id']}",
            'owner_id' => $repost['owner_id'],
            'date' => $repost['date'],
            'text' => $repost['text'],
            'attachments' => $this->parseAttachments($repost['attachments'])
        ];
    }

    /**
     * Метод парсинга прикреплений (attachments)
     * @param $attachs
     * @return array[]|null
     */
    private function parseAttachments($attachs) {
        if ($attachs) return null;

        /**
         * https://vk.com/dev/wall.post
         */
        $all_attachments = [
            'photo' => [],
            'video' => [],
            'audio' => [],
            'doc' => [],
            'link' => [],
//            'page' => [], // хз
//            'note' => [], // хз
            'poll' => [],
            'album' => [],
            'market' => [],
//            'market_album' => [],   // хз
//            'audio_playlist' => []  // link
        ];

        foreach ($attachs as $attach) {
            $a_type = $attach['type'];
            $a_attach = $attach[$a_type];

            if ($a_type == 'photo') {
                $last_size_url = $a_attach['sizes'][count($a_attach['sizes']) - 1]['url'];
                $photo_text = $a_attach['sizes'][count($a_attach['sizes']) - 1]['text'];
                $all_attachments[$a_type][] = [
                    'access_key' => $a_attach['access_key'],
                    'text'       => $photo_text,
                    'url'        => $last_size_url,
                    'ru_what_is' => 'Фото'
                ];

            } elseif ($a_type == 'video') {
                $all_attachments[$a_type][] = [
                    'access_key' => $a_attach['access_key'],
                    'title'      => $a_attach['title'],
                    // https://vk.com/videos95482342?z=video95482342_456239197
                    'url'        => "https://vk.com/videos{$a_attach['owner_id']}?z=video{$a_attach['owner_id']}_{$a_attach['id']}",
                    'ru_what_is' => 'Видео'
                ];

            } elseif ($a_type == 'audio') {
                $all_attachments[$a_type][] = [
                    'artist'     => $a_attach['artist'],
                    'title'      => $a_attach['title'],
                    'ru_what_is' => 'Аудио'
                ];

            } elseif ($a_type == 'doc') {
                $all_attachments[$a_type][] = [
                    'access_key' => $a_attach['access_key'],
                    'ext'        => $a_attach['ext'],
                    'title'      => $a_attach['title'],
                    'url'        => $a_attach['url'],
                    'ru_what_is' => 'Документ'
                ];

            } elseif ($a_type == 'link') {
                $all_attachments[$a_type][] = [
                    'description' => $a_attach['description'],
                    'caption'     => $a_attach['title'],
                    'url'         => $a_attach['url'],
                    'ru_what_is'  => 'Ссылка'
                ];

            } elseif ($a_type == 'poll') {
                $all_attachments[$a_type][] = [
                    'multiple'   => $a_attach['multiple'],
                    'question'   => $a_attach['question'],
                    'votes'      => $a_attach['votes'],
                    'answers'    => $a_attach['answers'],
                    'ru_what_is' => 'Опрос'
                ];
            } elseif ($a_type == 'album') {
                $owner_id = abs($a_attach['owner_id']);
                $all_attachments['album'][] = [
                    'url'         => "https://vk.com/public167866957?z=album{$owner_id}_{$a_attach['id']}",
                    'title'       => $a_attach['title'],
                    'description' => $a_attach['description'],
                    'size'        => $a_attach['size'],
                    'ru_what_is'  => 'Альбом'
                ];

            } elseif ($a_type == 'market') {
                $all_attachments[$a_type][] = [
                    'category_section' => "{$a_attach['category']['name']} / {$a_attach['category']['section']['name']}",
                    'title'            => $a_attach['title'],
                    'description'      => $a_attach['description'],
                    'url'              => "https://vk.com/market?w=product{$a_attach['owner_id']}_{$a_attach['id']}",
                    'thumb_photo'      => $a_attach['thumb_photo'],
                    'price'            => $a_attach['price']['text'],
                    'ru_what_is'       => 'Товар'
                ];
            }
        }

        return $all_attachments;
    }

    function makeContent()
    {
        // TODO: Implement makeContent() method.
    }

    function sendContent()
    {
        // TODO: Implement sendContent() method.
    }
}