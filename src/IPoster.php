<?php
namespace VKTOTG;


interface IPoster
{
    function getData($data);
    function makeContent();
    function sendContent();
}